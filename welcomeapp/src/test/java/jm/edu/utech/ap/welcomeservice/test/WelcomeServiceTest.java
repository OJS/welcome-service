package jm.edu.utech.ap.welcomeservice.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import jm.edu.utech.ap.welcomelib.IWelcomeService;
import jm.edu.utech.ap.welcomeservice.WelcomeService;

public class WelcomeServiceTest {
	@Test
	public void shouldReturnWelcomeMessage() {
		IWelcomeService service = new WelcomeService();
		String name = "John";
		String message = service
					.getWelcomeMessage(name);
	assertEquals(IWelcomeService.DEFAULT_WELCOME_PHRASE +name, message);
	}


}
