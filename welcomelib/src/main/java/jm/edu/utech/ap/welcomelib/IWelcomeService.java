package jm.edu.utech.ap.welcomelib;

/**
 * 
 * @author apstudent
 * */

public interface IWelcomeService {
	
	
	String DEFAULT_WELCOME_PHRASE 
		= "Thank You for coming ";
	

	/**
	 *Prepares and returns a <h1>welcome</h1> message
	 * @param name Name of person to welcome
	 * @return
	 **/
	String getWelcomeMessage(String name);

}
